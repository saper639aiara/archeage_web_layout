$(function() {
    //clone element for mobile version
    $('.section-banner-mobile').append($('.section-banner .info_banner').clone());

    $('.slider-news').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots:true,
        centerMode: false,
        variableWidth: false,
        prevArrow: '<button class="btn-prev slick-arrow"><span class="icon-prev"></span></button>',
        nextArrow: '<button class="btn-next slick-arrow"><span class="icon-next"></span></button>',
        responsive: [
            {
              breakpoint: 1900,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
              }
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 400,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows : false,
              }
            }
        ]
    });
})    