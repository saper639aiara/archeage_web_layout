$(function() {
    //click outside target
    window.addEventListener('mouseup', function(e) {
        $('.list_gnb').find('.item_gnb').removeClass('on');
        $('header').find('.logo').removeClass('on');
    });   
    //submenu open
    $('.list_gnb .item_gnb a').on('click', function(elem) {
        $(this).closest('.item_gnb').addClass('on');
        elem.preventDefault();
    }) 
    //sublogo menu open
    $('header .logo a').on('click', function(elem) {
        $(this).closest('.logo').addClass('on');
        elem.preventDefault();
    }) 
    //submenu hover and open menu
    $('.list_gnb .item_gnb a').hover(
        function(){ $(this).closest('.item_gnb').addClass('on') },
        function(){ $(this).closest('.item_gnb').removeClass('on') }
    )
    //submenu hover and open menu
    $('header .logo a').hover(
        function(){ $(this).closest('.logo').addClass('on') },
        function(){ $(this).closest('.logo').removeClass('on') }
    )
    //toogle mobile main menu    
    $('.section_menu .btn_togglemenu').on('click', function(){
        var $this = $(this);
        if(!$this.closest('header').hasClass('menu_on')){
            $this.closest('header').addClass('menu_on');
            $('html, body').addClass('overflow_on');
        }else{
            $this.closest('header').removeClass('menu_on');
            $('html, body').removeClass('overflow_on');
        }
    });
    //toogle mobile account menu
    // $('.section_account .btn_togglemenu').on('click', function(){
    //     var $this = $(this);
    //     if(!$this.closest('header').hasClass('account_on')){
    //         $this.closest('header').addClass('account_on');
    //         $('html, body').addClass('overflow_on');
    //     }else{
    //         $this.closest('header').removeClass('account_on');
    //         $('html, body').removeClass('overflow_on');
    //     }
    // });
    // Back to top button
    $(window).scroll(function() {
        if ($(this).scrollTop() > 100) {
        $('.back-to-top').fadeIn('slow');
        } else {
        $('.back-to-top').fadeOut('slow');
        }
    });
    $('.back-to-top').click(function(){
        $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
        return false;
    });
    setOptBox();
});

/**
 * @function
 * @summary 언어 선택 드롭다운 리스트
*/
function OptBox(selector, options) {
    this.selector,
        this.openBtn,
        this.openBtnHtml,
        this.openClassName,
        this.selectBtn,
        this.selectBtnCallback;
    this.init(selector, options);
    this.initEvent();
}

OptBox.prototype = {
    init: function (selector, options) {
        this.selector = $(selector);
        this.openBtn = this.selector.find(options.openBtn);
        this.openBtnHtml = this.openBtn.children(
            options.openBtnIcon
        )[0].outerHTML;
        this.openClassName = options.openClassName;
        this.selectBtn = this.selector.find(options.selectBtn);
        this.selectBtnCallback = options.selectBtnCallback;
    },
    initEvent: function () {
        var self = this;

        this.openBtn.on("click", function (e) {
            e.preventDefault();
            self.optOpenAt(this);
        });

        this.selectBtn.on("click", function (e) {
            e.preventDefault();
            if (typeof self.selectBtnCallback === "function") {
                self.selectBtnCallback(this);
            } else {
                self.selectBtnAt(this);
            }
        });
    },
    optOpenAt: function (evtThis) {
        var $evtThis = $(evtThis);

        if (this.selector.hasClass(this.openClassName)) {
            this.selector.removeClass(this.openClassName);
            $evtThis.attr("aria-expanded", "false");
        } else {
            this.selector.addClass(this.openClassName);
            $evtThis.attr("aria-expanded", "true");
        }
    },
    selectBtnAt: function (evtThis) {
        var $evtThis = $(evtThis);
        var thisText = $evtThis.text();

        this.openBtn
            .html(thisText + this.openBtnHtml)
            .attr("aria-expanded", "false");
        this.selector.removeClass(this.openClassName);
        this.selectBtn.attr("aria-selected", "false");
        $evtThis.attr("aria-selected", "true");
    }
};

$.fn.extend({
    optBox: function (setting) {
        return this.each(function () {
            var options = $.extend({}, $.fn.optBoxDefaults, setting || {});
            var optBox = new OptBox(this, options);
        });
    },
    optBoxDefaults: {
        openBtn: ".link_opt",
        openBtnIcon: ".ico_arrow",
        openClassName: "opt_open",
        selectBtn: ".link_option"
    }
});

function setOptBox() {
    $('.group_lang').optBox({
        openBtn: ".link_selected",
        openClassName: "on",
        selectBtn: ".link_lang",
        selectBtnCallback: function (evtThis) {
            var $evtThis = $(evtThis);
            var thisText = $evtThis.text();

            this.openBtn.attr("aria-expanded", "false");
            this.openBtn.children(".txt_selected").text(thisText);
            this.selector.removeClass(this.openClassName);
            this.selectBtn.attr("aria-selected", "false");
            $evtThis.attr("aria-selected", "true");
        }
    });
}